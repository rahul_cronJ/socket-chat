var app = angular.module("socketchat",['ui.router']);
app.config(["$stateProvider",function($stateProvider) {
  $stateProvider
    .state("home",{
      url:'/',
      templateUrl:'./templates/login.html',
      controller:'HomeController',
      controllerAs:"homeCtrl"
    })
    .state("chat",{
      url:'chatroom?user',
      templateUrl:'./templates/chat.html',
      controller:'ChatController',
      controllerAs:'chatCtrl'
    });
}]);
